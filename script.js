const tabsItems = document.querySelector('.tabs-bar');

function clickedTabs(event) {
    if (event.target.className === 'tabs-items') {
        const dataTab = event.target.getAttribute('data-tab');

        const tabsBar = document.getElementsByClassName('tabs-items');
        for (let i = 0; i < tabsBar.length; i++) {
            tabsBar[i].classList.remove('active');
        }
        event.target.classList.add('active');

        const tabItem = document.getElementsByClassName('tabs-content-items');
        for (let i = 0; i < tabItem.length; i++) {
            if (dataTab == i) {
                tabItem[i].style.display = 'block';
            } else {
                tabItem[i].style.display = 'none';
            }
        }
    }
}

tabsItems.addEventListener('click', clickedTabs);